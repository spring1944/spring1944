function widget:GetInfo()
	return {
		name = "Spring:1944 Ammo limiter",
		desc = "Feeds player-defined ammo limits to the game_ammo gadget",
		author = "ThinkSome",
		version   = "1.0",
		date = "20200223",
		license = "GNU AGPLv3",
		layer = 1000,
		enabled = true,
	}
end


local Chili, Screen0
local ammoWindow
local errorMessage


function widget:Initialize()

	if not WG.Chili then
		Spring.Echo("Error: Chili not found!")
		return
	end

	Chili = WG.Chili
	Screen0 = Chili.Screen0

	ammoWindow = Chili.Window:New {
		parent = Screen0,
		x = '80%',
		y = '20%',
		width = '15%',
		height = '10%',
		margin = {0,0,0,0}
	}

end


function widget:GameFrame(n)

	local selectedUnit = Spring.GetSelectedUnits()[1]
	if not selectedUnit then
		return
	end

	local unitDefID = Spring.GetUnitDefID(selectedUnit)
	local unitDef = UnitDefs[unitDefID]
	local name = unitDef.name

	if not unitDef.customParams or not unitDef.customParams.maxammo then
		return
	end

	local maxAmmo = unitDef.customParams.maxammo
	local ammo = Spring.GetUnitRulesParam(selectedUnit, "ammo")

	ammoWindow:ClearChildren()
	if not ammo then return end



	local image = Chili.Image:New {
		parent = ammoWindow,
		name   = "unitImage_" .. name,
		x = 0,
		y = 0,
		height = '50%',
		width = '20%',
		margin = {5, 5, 5, 5},
		file   = "unitpics/" .. unitDef.buildpicname
	}

	local ammoButton = Chili.Button:New {
		parent = ammoWindow,
		x = '20%',
		y = 0,
		height = '50%',
		width = '80%',
		margin = {5, 5, 5, 5},
		caption = "send",
		OnClick = {
			function(self)
				Spring.SendLuaRulesMsg('\139' .. unitDefID .. ":" .. 5)
			end
		},
		font = {
			size = 16,
			color = {1,0.2,0.2,1},
		}
	}

	local ammoImage = Chili.Image:New {
		parent = ammoWindow,
		name   = "ammoImage",
		x = 0,
		y = '50%',
		width = '20%',
		height = '50%',
		margin = {5, 5, 5, 5},
		file   = "LuaUI/Images/Bitmaps/ResLogIcon.png"
	}

	local ammoBar = Chili.Progressbar:New {
		parent = ammoWindow,
		x = '20%',
		y = '50%',
		width = '80%',
		height = '50%',
		margin = {5, 5, 5, 5},
		min = 0,
		max = maxAmmo,
		value = ammo,
		caption = "ammo " .. ammo .. "/" .. maxAmmo,
		color = {1.0, 1.0, 0, 0.8}
	}

end
